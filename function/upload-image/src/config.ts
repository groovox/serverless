import { ClientOptions } from "minio";

export interface S3Config extends ClientOptions {
  bucket: string;
}

export interface Config {
  s3: S3Config;
}

export const readConfig = (): Config => {
  const {
    AWS_REGION: defaultRegion = "",
    AWS_ACCESS_KEY_ID: defaultAccessKey = "",
    AWS_SECRET_ACCESS_KEY: defaultSecretKey = "",
    AWS_SESSION_TOKEN: defaultSessionToken
  } = process.env;
  const {
    S3_ENDPOINT: endPoint = "s3.amazonaws.com",
    S3_REGION: region = defaultRegion,
    S3_BUCKET: bucket = "",
    S3_ACCESS_KEY: accessKey = defaultAccessKey,
    S3_SECRET_KEY: secretKey = defaultSecretKey,
    S3_SESSION_TOKEN: sessionToken = defaultSessionToken
  } = process.env;

  const s3: S3Config = {
    endPoint,
    region,
    bucket,
    accessKey,
    secretKey,
    sessionToken
  };
  return {
    s3
  };
};
