export interface Query {
  key: string;
  /**
   * In MB
   */
  maxSize?: number;
  /**
   * In seconds
   */
  expire?: number;
}
