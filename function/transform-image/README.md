# @groovox/transform-image

Transforms images from S3 bucket runtime and secure by JSON web token (JWT).

You should have a CDN to cache the result of the lambda for the best performance.

> It is recommend to assign more memory (1536 MB is recommended by Sharp) for better performance.

## Environment Variables

- `S3_ENDPOINT`: default to `s3.amazonaws.com`
- `S3_REGION`: default to `AWS_REGION`
- `S3_BUCKET`
- `S3_ACCESS_KEY`: default to `AWS_ACCESS_KEY_ID`
- `S3_SECRET_KEY`: default to `AWS_SECRET_ACCESS_KEY`
- `JWT_SECRET`

If you use the default configuration, the execution role of the lambda must have `s3:GetObject` for the bucket.

## Query Parameters

`q` should be a JWT that contains the following payload. It contains the following values.

### key

Key of the image from S3 bucket.

### image

Query passed to [sharp](https://sharp.pixelplumbing.com). It should be a JSON array where the first element is the method name and the rest are arguments.

It is executed in the array order.`toFormat` is mandatory and is must be the last element. Format `origin` will return the origin image.

```json
{
  "key": "a.png",
  "image": [
    ["resize", 300],
    ["toFormat", "webp"]
  ]
}
```
