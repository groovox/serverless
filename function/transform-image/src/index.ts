import { createHandler, ImageQuery } from "@lambda-sharp/static";

const person: ImageQuery = {
  queries: [{ method: "resize", params: [{ width: 400, height: 400 }] }],
  output: {
    webp: [],
    png: [],
    jpeg: []
  }
};

const background: ImageQuery = {
  queries: [{ method: "resize", params: [{ width: 1920, height: 1080 }] }],
  output: {
    webp: [],
    png: [],
    jpeg: []
  }
};

const poster: ImageQuery = {
  queries: [{ method: "resize", params: [{ width: 1000, height: 1500 }] }],
  output: {
    webp: [],
    png: [],
    jpeg: []
  }
};

const cover: ImageQuery = {
  queries: [{ method: "resize", params: [{ width: 1000, height: 1000 }] }],
  output: {
    webp: [],
    png: [],
    jpeg: []
  }
};

export const handler = createHandler({
  person,
  background,
  poster,
  cover
});
